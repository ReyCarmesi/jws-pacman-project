﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWS_PACMAN
{
    public class Estado
    {
        public int[,] MapaActual { get; set; }
        public Pacman Pacman { get; set; }
        public List<Fantasma> Fantasmas { get; set; }
        public int Puntaje { get; set; }
        public int NumComidos { get; set; }
        public EstadoJuego EstadoJuego { get; set; }
        public ModoFantasmas ModoFantasmas { get; set; }
        public bool PoderActivado { get; set; }
        public int DuracionPoder { get; set; }
    }
}
