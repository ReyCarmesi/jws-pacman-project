﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWS_PACMAN
{
    public class Constantes
    {
        //Valores de una Celda
        public const int MURO = 1;
        public const int CAMINO = 0;
        public const int COMIDA = 2;
        public const int PODER = 3;

        //Dimensiones del Mapa
        public const int LARGO = 31;
        public const int ANCHO = 28;

        //Lista de Direcciones
        public static List<Direccion> Direcciones = new List<Direccion> { Direccion.ABAJO, Direccion.ARRIBA, Direccion.DERECHA, Direccion.IZQUIERDA };
    }

    public enum Personalidad { BLINKY, INKY, PINKY, CLYDE } //Usado en los Fantasmas
    public enum Direccion { ARRIBA, ABAJO, DERECHA, IZQUIERDA, NULL } //Usado en los Fantasmas y el Pacman
    public enum EstadoFantasma { NORMAL, COMIBLE, COMIDO } //Definirá si el fantasma ha sido comido o si puede ser comido
    public enum EstadoJuego { JUEGO, GANAR, MUERTE} //Usado en el Gestor
    public enum ModoIA { CAZA, TRANSICION, ESCAPE} //Usado para la evaluacion de movimientos de los Fantasmas
    public enum ModoFantasmas { ALEATORIO, ESTANDAR, AGRESIVO } //Usado como un estado del juego
    public enum ModoAcercamiento { MANHATTAN, EUCLIDES} //Usado en el gestor para el calculo de la distancia entre posiciones
}
