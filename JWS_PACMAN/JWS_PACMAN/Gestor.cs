﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace JWS_PACMAN
{
    public class Gestor
    {
        #region ---ATRIBUTOS DEL GESTOR---
        public int[,] Mapa { get; set; }
        public Pacman Pacman { get; set; }
        public List<Fantasma> Fantasmas { get; set; }
        public int Puntaje { get; set; } //Numero que aumentará dependiendo de las comidas recogidas, fantasmas comidos y frutas
        public int NumComidos {get; set;} //Numero que determina cuanto de comidas ha recogido y le falta para acabar el nivel
        public int ComidaMax { get; set; } //Valor que se establecerá al generar un nivel y servirá para terminar el nivel
        public EstadoJuego EstadoJuego { get; set; } //Establece si el juego está en curso, gana o muere
        public bool PacmanBotActivado { get; set; } //Bot o controles manuales
        public ModoAcercamiento ModoAcercamiento { get; set; } //Determina la forma de calcular las distancias entre los fantasmas y Pacman
        public ModoFantasmas ModoFantasmas { get; set; } //Estado que determina el comportamiento de los fantasmas
        public List<V> Vertices { get; set; }
        public bool mostrarVertices { get; set; }
        public bool mostrarObjetivos { get; set; }
        public int contadorPoder { get; set; }
        public bool poderActivado { get; set; }
        public bool MapaClasico { get; set; }
        public Color MapaColor { get; set; }
        public int Profundidad { get; set; }
        #endregion

        public Gestor()
        {
            Mapa = new int[Constantes.LARGO, Constantes.ANCHO];
            Puntaje = 0;
            NumComidos = 0;            
            Fantasmas = new List<Fantasma>();
            Vertices = new List<V>();
            PacmanBotActivado = false;
            poderActivado = false;
            Pacman = new Pacman(Direccion.DERECHA, new Tuple<int, int>(14, 23));
            Fantasmas.Add(new Fantasma(Personalidad.BLINKY, new Tuple<int, int>(14, 11)));
            Fantasmas.Add(new Fantasma(Personalidad.INKY, new Tuple<int, int>(12, 14)));
            Fantasmas.Add(new Fantasma(Personalidad.PINKY, new Tuple<int, int>(14, 14)));
            Fantasmas.Add(new Fantasma(Personalidad.CLYDE, new Tuple<int, int>(16, 14)));
        }

        /// <summary>
        /// OPERACIONES DE CARGA DE MAPA Y REINICIO DE NIVEL
        /// </summary>
        #region ---PREINICIO---
        public void reiniciarJuego()
        {
            Pacman.Posicion = new Tuple<int, int>(14, 23); //Posicion por defecto
            Pacman.Direccion = Direccion.DERECHA; //Pacman siempre empezará moviendose hacie la derecha
            Fantasmas.Clear();
            Fantasmas.Add(new Fantasma(Personalidad.BLINKY, new Tuple<int, int>(14, 11)));
            Fantasmas.Add(new Fantasma(Personalidad.INKY, new Tuple<int, int>(12, 14)));
            Fantasmas.Add(new Fantasma(Personalidad.PINKY, new Tuple<int, int>(14, 14)));
            Fantasmas.Add(new Fantasma(Personalidad.CLYDE, new Tuple<int, int>(16, 14)));
            ModoFantasmas = ModoFantasmas.ESTANDAR;
            Puntaje = 0;
            NumComidos = 0;
            EstadoJuego = EstadoJuego.JUEGO; //Inicia el Juego
        }
        public void generarMapa()
        {
            //MAPA CLASICO
            if (MapaClasico)
            {
                int[,] MapaLlenar = Mapas.MAPACLASICO;
                for (int i = 0; i < Constantes.LARGO; ++i )
                {
                    for(int j=0; j<Constantes.ANCHO; ++j)
                    {
                        Mapa[i, j] = MapaLlenar[i, j];
                    }
                }

                    MapaColor = Color.Blue;
            }
            else
            {
                Random r = new Random();
                int [,] MapaLlenar = Mapas.ListaMapas[r.Next(Mapas.ListaMapas.Count())]; //Escoge cualquier mapa de la lista
                for(int i=0; i<Constantes.LARGO; ++i)
                {
                    for(int j=0; j<Constantes.ANCHO; ++j)
                    {
                        Mapa[i, j] = MapaLlenar[i, j];
                    }
                }
                MapaColor = Mapas.ListaColores[r.Next(Mapas.ListaColores.Count())];
            }

            //Al ser las comidas esparcidas por el mapa un valor constante, se calcula al momento de generar el mapa
            //para evitar el conteo de las mismas por cada iteración
            establecerMaxComida();
            //Forma el grafo de esquinas
            formarGrafo();
        }

        private void establecerMaxComida()
        {
            ComidaMax = 0;

            for (int i = 0; i < Constantes.LARGO; ++i)
            {
                for (int j = 0; j < Constantes.ANCHO; ++j)
                    if (Mapa[i, j] == Constantes.COMIDA) ComidaMax += 1;
            }
        }
        private void formarGrafo()
        {
            Vertices.Clear();

            for (int i = 1; i < Constantes.LARGO - 1; ++i)
            {
                for (int j = 1; j < Constantes.ANCHO - 1; ++j)
                {
                    if (Mapa[i, j] != Constantes.MURO && !((j>=11 && j<=16) && (i>=13 && i<=15)))
                    {
                        //Booleanos que determinan si las celdas aledañas están libres
                        bool libreArriba = (Mapa[i - 1, j] != Constantes.MURO);
                        bool libreAbajo = (Mapa[i + 1, j] != Constantes.MURO);
                        bool libreIzquierda = (Mapa[i, j - 1] != Constantes.MURO);
                        bool libreDerecha = (Mapa[i, j + 1] != Constantes.MURO);

                        if ((libreAbajo || libreArriba) && (libreDerecha || libreIzquierda)) //Determina si es una esquina
                            Vertices.Add(new V(new Tuple<int, int>(j, i)));
                    }
                }
            }

            foreach (V v in Vertices)
            {
                //Booleanos que determinan si las celdas aledañas están libres
                bool libreArriba = (Mapa[v.valor.Item2 - 1, v.valor.Item1] != Constantes.MURO);
                bool libreAbajo = (Mapa[v.valor.Item2 + 1, v.valor.Item1] != Constantes.MURO);
                bool libreIzquierda = (Mapa[v.valor.Item2, v.valor.Item1 - 1] != Constantes.MURO);
                bool libreDerecha = (Mapa[v.valor.Item2, v.valor.Item1 + 1] != Constantes.MURO);

                if (libreArriba)
                {
                    //Obtendrá el vértice que se encuentre en su misma columna, filtrará los que tengan menor Y (Arriba suyo) y cogerá el que tenga el mayor valor de Y (Mayor a Menor)
                    V VerticeArriba = Vertices.Where(x => x.valor.Item1 == v.valor.Item1 && x.valor.Item2 < v.valor.Item2).OrderByDescending(x => x.valor.Item2).First();
                    v.anexos.Add(new E(VerticeArriba, Direccion.ARRIBA));
                }
                if (libreAbajo)
                {
                    //Obtendrá el vértice que se encuentre en su misma columna, filtrará los que tengan mayor Y (Abajo suyo) y cogerá el que tenga el menor valor de Y (Menor a Mayor)
                    V VerticeAbajo = Vertices.Where(x => x.valor.Item1 == v.valor.Item1 && x.valor.Item2 > v.valor.Item2).OrderBy(x => x.valor.Item2).First();
                    v.anexos.Add(new E(VerticeAbajo, Direccion.ABAJO));
                }
                if (libreDerecha)
                {
                    //Obtendrá el vértice que se encuentre en su misma fila, filtrará los que tengan mayor X (Derecha suyo) y cogerá el que tenga el menor valor de X (Menor a Mayor)
                    V VerticeDerecho = null;

                    List<V> Vs = Vertices.Where(x => x.valor.Item2 == v.valor.Item2 && x.valor.Item1 > v.valor.Item1).OrderBy(x => x.valor.Item1).ToList();
                    if (Vs.Count() == 0)
                        VerticeDerecho = Vertices.Where(x => x.valor.Item2 == v.valor.Item2 && x.valor.Item1 < v.valor.Item1).OrderByDescending(x => x.valor.Item1).Last(); //Si es un portal selecciona el del otro extremo
                    else
                        VerticeDerecho = Vs.First();

                    v.anexos.Add(new E(VerticeDerecho, Direccion.DERECHA));
                }
                if (libreIzquierda)
                {
                    //Obtendrá el vértice que se encuentre en su misma fila, filtrará el que tenga menor X (Izquierda suyo) y cogerá el que tenga el mayor valor de X (Mayor a menor)
                    V VerticeIzquierdo = null;

                    List<V> Vs = Vertices.Where(x => x.valor.Item2 == v.valor.Item2 && x.valor.Item1 < v.valor.Item1).OrderByDescending(x => x.valor.Item1).ToList();
                    if (Vs.Count() == 0)
                        VerticeIzquierdo = Vertices.Where(x => x.valor.Item2 == v.valor.Item2 && x.valor.Item1 > v.valor.Item1).OrderBy(x => x.valor.Item1).Last(); //Si es un portal selecciona el del otro extremo
                    else
                        VerticeIzquierdo = Vs.First();

                    v.anexos.Add(new E(VerticeIzquierdo, Direccion.IZQUIERDA));
                }
            }
        }
        #endregion

        /// <summary>
        /// OPERACIONES DE DIBUJADO EN EL CANVAS
        /// </summary>
        #region ---DIBUJADO---
        public void dibujar(Graphics g)
        {
            if (mostrarVertices)
                dibujarVertices(g);

            if (mostrarObjetivos)
                dibujarObjetivos(g);

            dibujarMapa(g);
            dibujarFantasmas(g);
            Pacman.dibujar(g);
            dibujarFilasColumnas(g);
        }
        private void dibujarFilasColumnas(Graphics g)
        {
            for (int i = 0; i < Constantes.LARGO; ++i)
                g.DrawString(i.ToString(), new Font("Arial", 8), Brushes.White, Constantes.ANCHO * 15, i * 15);
            for (int j = 0; j < Constantes.ANCHO; ++j)
                g.DrawString(j.ToString(), new Font("Arial", 8), Brushes.White, j * 15, Constantes.LARGO * 15);
        }
        private void dibujarMapa(Graphics g)
        {
            for (int i = 0; i < Constantes.LARGO; ++i)
            {
                for (int j = 0; j < Constantes.ANCHO; ++j)
                {
                    if (Mapa[i, j] == Constantes.MURO)
                        g.FillRectangle(new SolidBrush(MapaColor), j * 15, i * 15, 15, 15);

                    if (Mapa[i, j] == Constantes.COMIDA)
                        g.FillEllipse(Brushes.Yellow, j * 15 + 15 / (2 + 3 / 2), i * 15 + 15 / (2 + 3 / 2), 15 / 4, 15 / 4);

                    if (Mapa[i, j] == Constantes.PODER)
                        g.FillEllipse(Brushes.White, j * 15 + 15 / 8, i * 15 + 15 / 8, 3 * (15 / 4), 3 * (15 / 4));
                }
            }
        }

        private void dibujarFantasmas(Graphics g)
        {
            foreach (Fantasma f in Fantasmas)
                f.dibujar(g);
        }

        private void dibujarVertices(Graphics g)
        {
            foreach (V v in Vertices)
                g.FillRectangle(new SolidBrush(Color.Green), v.valor.Item1 * 15, v.valor.Item2 * 15, 15, 15);
        }

        private void dibujarObjetivos(Graphics g)
        {
            foreach(Fantasma f in Fantasmas)
            {
                Color color = Color.White;
                switch(f.Personalidad)
                {
                    case Personalidad.BLINKY:
                        color = Color.Red;
                        break;
                    case Personalidad.INKY:
                        color = Color.Aquamarine;
                        break;
                    case Personalidad.CLYDE:
                        color = Color.Orange;
                        break;
                    case Personalidad.PINKY:
                        color = Color.Pink;
                        break;
                }
                g.FillRectangle(new SolidBrush(color), f.PosicionObjetivo.Item1 * 15, f.PosicionObjetivo.Item2 * 15, 15, 15);
            }
        }
        #endregion

        /// <summary>
        /// MOVIMIENTO DE LA LISTA DE FANTASMAS Y DE PACMAN
        /// </summary>
        #region ---MOVIMIENTOS DE LAS ENTIDADES---
        public void moverTodo()
        {
            Random r = new Random();

            if (PacmanBotActivado)
                minimax(Profundidad);

            moverPacman();
            transcurrirJuego();
            validarColision();
            moverFantasmas();
            validarColision();
        }
        public void transcurrirJuego()
        {
            //Evalua la posicion si está comiendo y sube el puntaje
            if (Mapa[Pacman.Posicion.Item2, Pacman.Posicion.Item1] == Constantes.COMIDA)
            {
                Puntaje += 10;
                NumComidos += 1;
                Mapa[Pacman.Posicion.Item2, Pacman.Posicion.Item1] = Constantes.CAMINO;

                if (NumComidos == ComidaMax) //FIN DEL JUEGO
                    EstadoJuego = EstadoJuego.GANAR;
            }
            //Activa poder
            if (Mapa[Pacman.Posicion.Item2, Pacman.Posicion.Item1] == Constantes.PODER)
            {
                Mapa[Pacman.Posicion.Item2, Pacman.Posicion.Item1] = Constantes.CAMINO;
                Puntaje += 30;

                foreach (Fantasma f in Fantasmas)
                {
                    f.Estado = EstadoFantasma.COMIBLE;
                    f.ModoIA = ModoIA.ESCAPE;
                    this.contadorPoder = 0; //Inicia contador para que acabe poder
                    this.poderActivado = true;
                }
            }
            if (poderActivado)
            {
                if (contadorPoder >= 150)
                {
                    foreach (Fantasma f in Fantasmas) //todo
                    {
                        f.Estado = EstadoFantasma.NORMAL;
                        f.ModoIA = ModoIA.CAZA;
                    }
                    poderActivado = false;
                    contadorPoder = 0;
                }

                ++contadorPoder;
            }
        }
        //Funcion que produce el movimiento constante del Pacman y que evalua si gana o muere el Juego
        private void moverPacman()
        {
            int dx = (Pacman.Direccion == Direccion.DERECHA) ? 1 : ((Pacman.Direccion == Direccion.IZQUIERDA) ? -1 : 0);
            int dy = (Pacman.Direccion == Direccion.ABAJO) ? 1 : ((Pacman.Direccion == Direccion.ARRIBA) ? -1 : 0);
            Tuple<int, int> posicionFutura = new Tuple<int, int>(Pacman.Posicion.Item1 + dx, Pacman.Posicion.Item2 + dy);

            if (Pacman.Posicion.Item1 == 0 && dx == -1) //Portal Izquierdo
            {
                Pacman.Posicion = new Tuple<int, int>(Constantes.ANCHO - 1, Pacman.Posicion.Item2);
                return;
            }
            if (Pacman.Posicion.Item1 == Constantes.ANCHO - 1 && dx == 1) //Portal Derecho
            {
                Pacman.Posicion = new Tuple<int, int>(0, Pacman.Posicion.Item2);
                return;
            }

            if (movimientoValido(posicionFutura))
                Pacman.Posicion = posicionFutura; //Ejecuta el movimiento

        }
        //Funcion que mueve todos los fantasmas (4)
        public void moverFantasmas()
        {
            if (NumComidos == ComidaMax / 4) Fantasmas[1].Posicion = new Tuple<int, int>(14, 11);
            if (NumComidos == ComidaMax / 3) Fantasmas[2].Posicion = new Tuple<int, int>(14, 11);
            if (NumComidos == ComidaMax / 2) Fantasmas[3].Posicion = new Tuple<int, int>(14, 11);

            foreach (Fantasma f in Fantasmas)
            {
                moverFantasma(f, escogerMovimiento(Pacman, f, ModoFantasmas));
            }
        }
        //Funcion que mueve al Fantasma individualmente
        public void moverFantasma(Fantasma fantasma, Direccion direccion)
        {
            int dx = (direccion == Direccion.DERECHA) ? 1 : ((direccion == Direccion.IZQUIERDA) ? -1 : 0);
            int dy = (direccion == Direccion.ABAJO) ? 1 : ((direccion == Direccion.ARRIBA) ? -1 : 0);
            Tuple<int, int> posicionFutura = new Tuple<int, int>(fantasma.Posicion.Item1 + dx, fantasma.Posicion.Item2 + dy);

            if (fantasma.Posicion.Item1 == 0 && dx == -1) //Portal Izquierdo
            {
                fantasma.Posicion = new Tuple<int, int>(Constantes.ANCHO - 1, fantasma.Posicion.Item2);
                return;
            }
            if (fantasma.Posicion.Item1 == Constantes.ANCHO - 1 && dx == 1) //Portal Derecho
            {
                fantasma.Posicion = new Tuple<int, int>(0, fantasma.Posicion.Item2);
                return;
            }
            if (movimientoValido(posicionFutura))
            {
                fantasma.Posicion = posicionFutura;
                fantasma.Direccion = direccion;
            }
        }
        #endregion

        /// <summary>
        /// VALIDAR MUROS Y COLISIONES ENTRE FANTASMAS CON PACMAN
        /// </summary>
        #region ---FUNCIONES DE VALIDACION DE MOVIMIENTOS---
        private void validarColision() //Funcion que evalua la muerte de pacman o si los fantasmas son comidos
        {
            List<Fantasma> fantasmasColisionados = Fantasmas.Where(x => x.Posicion.Item1 == Pacman.Posicion.Item1 && x.Posicion.Item2 == Pacman.Posicion.Item2).ToList();
            if (fantasmasColisionados.Count() > 0)
            {
                foreach(Fantasma f in fantasmasColisionados)
                {
                    if (f.Estado == EstadoFantasma.NORMAL)
                    {
                        EstadoJuego = EstadoJuego.MUERTE;
                        Puntaje -= 500;
                    }
                    if (f.Estado == EstadoFantasma.COMIBLE) 
                    { 
                        f.Estado = EstadoFantasma.COMIDO; 
                        Puntaje += 100; 
                    }
                }
            }
        }
        //Funcion que evalua que una posicion futura no traspase el muro y que pueda entrar por el portal
        bool movimientoValido(Tuple<int, int> posicionFutura)
        {
            if (posicionFutura.Item1 < 0 || posicionFutura.Item1 >= Constantes.ANCHO) return true; //Portal Izquierdo y Derecho
            return Mapa[posicionFutura.Item2, posicionFutura.Item1] != Constantes.MURO;
        }
        #endregion

        /// <summary>
        /// MOVIMIENTOS DE LOS FANTASMAS POR EL GRAFO DE ESQUINAS Y LAS FUNCIONES DE TRIANGULACION
        /// </summary>
        #region ---IA DE FANTASMAS---
        //Funcion que escoge el movimiento apropiado para cada fantasma dependiendo de su personalidad (ver Doc.)
        public Direccion escogerMovimiento(Pacman pacman, Fantasma fantasma, ModoFantasmas modoFantasmas)
        {
            Random r = new Random();
            V VerticeActual = Vertices.SingleOrDefault(x => x.valor.Item1 == fantasma.Posicion.Item1 && x.valor.Item2 == fantasma.Posicion.Item2);

            if (VerticeActual == null && fantasma.UltimoVertice == null) return r.Next(0, 1) == 0 ? Direccion.DERECHA : Direccion.IZQUIERDA;

            if (VerticeActual != null)
            {
                Tuple<int, int> PosicionTriangular = new Tuple<int, int>(0, 0);
                
                //ModoIA ESTANDAR
                if (modoFantasmas != ModoFantasmas.ALEATORIO)
                {
                    int aleatoriedad = r.Next(0, 10);
                    switch (fantasma.Personalidad)
                    {
                        case Personalidad.BLINKY:
                            {
                                //Triangulará la posición actual de Pacman
                                PosicionTriangular = new Tuple<int, int>(pacman.Posicion.Item1, pacman.Posicion.Item2);
                                break;
                            }
                        case Personalidad.INKY:
                            {
                                //Triangulará la posición 2 celdas adelante de Pacman
                                int dx = (pacman.Direccion == Direccion.DERECHA) ? 2 : ((pacman.Direccion == Direccion.IZQUIERDA) ? -2 : 0);
                                int dy = (pacman.Direccion == Direccion.ABAJO) ? 2 : ((pacman.Direccion == Direccion.ARRIBA) ? -2 : 0);

                                if (pacman.Posicion.Item1 + dx <= 0 || pacman.Posicion.Item2 + dx >= Constantes.ANCHO || pacman.Posicion.Item2 + dy <= 0 || pacman.Posicion.Item2 + dy >= Constantes.LARGO)
                                    PosicionTriangular = new Tuple<int, int>(pacman.Posicion.Item1, pacman.Posicion.Item2);
                                else
                                    PosicionTriangular = new Tuple<int, int>(pacman.Posicion.Item1 + dx, pacman.Posicion.Item2 + dy);

                                break;
                            }
                        case Personalidad.PINKY:
                            {
                                //Triangulará la posición 2 celdas atrás de Pacman
                                int dx = (pacman.Direccion == Direccion.DERECHA) ? -2 : ((pacman.Direccion == Direccion.IZQUIERDA) ? 2 : 0);
                                int dy = (pacman.Direccion == Direccion.ABAJO) ? -2 : ((pacman.Direccion == Direccion.ARRIBA) ? 2 : 0);

                                if (pacman.Posicion.Item1 + dx <= 0 || pacman.Posicion.Item2 + dx >= Constantes.ANCHO || pacman.Posicion.Item2 + dy <= 0 || pacman.Posicion.Item2 + dy >= Constantes.LARGO)
                                    PosicionTriangular = new Tuple<int, int>(pacman.Posicion.Item1, pacman.Posicion.Item2);
                                else
                                    PosicionTriangular = new Tuple<int, int>(pacman.Posicion.Item1 + dx, pacman.Posicion.Item2 + dy);

                                break;
                            }
                        case Personalidad.CLYDE:
                            {
                                //Triangulará una posición aleatoria del mapa
                                PosicionTriangular = new Tuple<int, int>(r.Next(1, Constantes.ANCHO), r.Next(1, Constantes.LARGO));
                                break;
                            }
                    }

                    //Activará el factor de aleatoriedad solo en caso sea un modo de juego ESTANDAR
                    if (modoFantasmas == ModoFantasmas.ESTANDAR)
                    {
                        //Factor de Aleatoriedad
                        if (aleatoriedad > 4 && aleatoriedad < 8)
                        {
                            fantasma.ModoIA = (!poderActivado) ? ModoIA.TRANSICION : ModoIA.ESCAPE; //Valida que no se cambie el estado de la IA mientras el poder está activo
                            PosicionTriangular = new Tuple<int, int>(r.Next(1, Constantes.ANCHO), r.Next(1, Constantes.LARGO));
                        }
                        else
                            fantasma.ModoIA = (!poderActivado) ? ModoIA.CAZA : ModoIA.ESCAPE; //Valida que no se cambie el estado de la IA mientras el poder está activo
                    }
                }

                //En el modo de juego ALEATORIO todos los fantasmas triangularán posiciones aleatorias del mapa
                if (modoFantasmas == ModoFantasmas.ALEATORIO)
                {
                    fantasma.ModoIA = ModoIA.TRANSICION;
                    PosicionTriangular = new Tuple<int, int>(r.Next(1, Constantes.ANCHO), r.Next(1, Constantes.LARGO));
                }

                Direccion direccionDestino = Direccion.DERECHA;
                double distancia = (fantasma.ModoIA == ModoIA.ESCAPE) ? Double.MinValue : Double.MaxValue;
                
                //Recorre los vértices anexos del vértice donde se encuentra actualmente
                foreach(E e in VerticeActual.anexos)
                {
                    //Dependiendo de la configuración elegirá un tipo de calculo u otro
                    double distanciaActual = (ModoAcercamiento == ModoAcercamiento.EUCLIDES) ? calcularDistanciaEuclid(PosicionTriangular, e.vertice.valor)
                : calcularDistanciaManhattan(PosicionTriangular, e.vertice.valor); //Calculará la distancia que hay entre la posición a triangular y el vértice a donde piensa moverse

                    bool esBucle = fantasma.UltimoVertice == e.vertice; //Valida que no vuelva a su misma posición

                    if (fantasma.ModoIA != ModoIA.ESCAPE) //En caso no esté en modo ESCAPE, el fantasma buscará acercarse, cogiendo la menor distancia
                    {
                        if (distanciaActual < distancia && !esBucle)
                        {
                            distancia = distanciaActual;
                            direccionDestino = e.direccion;
                        }
                    }
                    else //En caso esté en modo ESCAPE, el fantasma buscará alejarse, cogiendo la mayor distancia
                    {
                        if (distanciaActual > distancia && !esBucle)
                        {
                            distancia = distanciaActual;
                            direccionDestino = e.direccion;
                        }
                    }
                }

                fantasma.PosicionObjetivo = PosicionTriangular; //Se guarda la posición triangulada para fines gráficos
                fantasma.UltimoVertice = VerticeActual; //El vértice visitado se guarda
                return direccionDestino;
            }

            return fantasma.Direccion;
        }

        private double calcularDistanciaManhattan(Tuple<int,int> Pos1, Tuple<int,int> Pos2)
        {
            int dx = Math.Abs(Pos1.Item1 - Pos2.Item1);
            int dy = Math.Abs(Pos1.Item2 - Pos2.Item2);
            return dx + dy;
        }
        private double calcularDistanciaEuclid(Tuple<int,int> Pos1, Tuple<int,int> Pos2)
        {
            double dx = Pos1.Item1 - Pos2.Item1;
            double dy = Pos1.Item2 - Pos2.Item2;
            return Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
        }
        #endregion

        #region ---IA DE PACMAN---
        private bool hayMuro(Tuple<int,int> posicion, int[,] mapaEv)
        {
            if (posicion.Item1 < 0 || posicion.Item1 >= Constantes.ANCHO) return false;
            return mapaEv[posicion.Item2, posicion.Item1] == Constantes.MURO;
        }

        private List<Direccion> obtPacmanMovs(Pacman pacman, int[,] mapaEv)
        {
            List<Direccion> movs = new List<Direccion>();
            foreach(Direccion d in Constantes.Direcciones)
            {
                int dx = (d == Direccion.DERECHA) ? 1 : ((d == Direccion.IZQUIERDA) ? -1 : 0);
                int dy = (d == Direccion.ABAJO) ? 1 : ((d == Direccion.ARRIBA) ? -1 : 0);
                Tuple<int, int> posicionFutura = new Tuple<int, int>(pacman.Posicion.Item1 + dx, pacman.Posicion.Item2 + dy);

                if (!hayMuro(posicionFutura, mapaEv))
                    movs.Add(d);
            }
            return movs;
        }
        private List<Direccion> obtFantasmaMovs(Fantasma f, int[,] mapaEv)
        {
            List<Direccion> movs = new List<Direccion>();
            foreach (Direccion d in Constantes.Direcciones)
            {
                int dx = (d == Direccion.DERECHA) ? 1 : ((d == Direccion.IZQUIERDA) ? -1 : 0);
                int dy = (d == Direccion.ABAJO) ? 1 : ((d == Direccion.ARRIBA) ? -1 : 0);
                Tuple<int, int> posicionFutura = new Tuple<int, int>(f.Posicion.Item1 + dx, f.Posicion.Item2 + dy);

                if (!hayMuro(posicionFutura, mapaEv))
                    movs.Add(d);
            }
            return movs;
        }

        public Estado genNuevoEstado(Direccion d, int id, Pacman pacman, List<Fantasma> fs, int Puntaje, int[,] Mapa, EstadoJuego est, int NumComidos, ModoFantasmas modoFs, bool PoderActivado, int DuracionPoder)
        {
            Estado estado = new Estado();

            Pacman nuevoPacman = new Pacman(pacman.Direccion, pacman.Posicion);
            List<Fantasma> nuevosFs = new List<Fantasma>();
            foreach(Fantasma f in fs)
            {
                Fantasma nuevo = new Fantasma(f.Personalidad, f.Posicion);
                nuevo.ModoIA = f.ModoIA;
                nuevo.PosicionObjetivo = f.PosicionObjetivo;
                nuevo.UltimoVertice = f.UltimoVertice;
                nuevo.Direccion = f.Direccion;
                nuevo.Estado = f.Estado;
                nuevosFs.Add(nuevo);
            }

            int nuevoPuntaje = Puntaje;
            EstadoJuego nuevoEst = est;
            int nuevoNumCom = NumComidos;
            ModoFantasmas nuevoModoFs = modoFs;
            bool nuevoPoderActivado = poderActivado;
            int nuevoDuracionPoder = DuracionPoder;
            int[,] nuevoMapa = new int[Constantes.LARGO, Constantes.ANCHO];
            
            nuevoMapa = (int[,])Mapa.Clone();

            int dx = (d == Direccion.DERECHA) ? 1 : ((d == Direccion.IZQUIERDA) ? -1 : 0);
            int dy = (d == Direccion.ABAJO) ? 1 : ((d == Direccion.ARRIBA) ? -1 : 0);

            if(id == 0)
            {
                nuevoPacman.Posicion = new Tuple<int,int>(nuevoPacman.Posicion.Item1 + dx, nuevoPacman.Posicion.Item2 + dy);
                nuevoPacman.Direccion = d;

                if (nuevoPacman.Posicion.Item1 <= 0 && dx == -1) //Portal Izquierdo
                    nuevoPacman.Posicion = new Tuple<int, int>(Constantes.ANCHO - 1, nuevoPacman.Posicion.Item2);

                if (nuevoPacman.Posicion.Item1 >= Constantes.ANCHO - 1 && dx == 1) //Portal Derecho
                    nuevoPacman.Posicion = new Tuple<int, int>(0, nuevoPacman.Posicion.Item2);
                
                if (nuevoMapa[nuevoPacman.Posicion.Item2, nuevoPacman.Posicion.Item1] == Constantes.COMIDA)
                {
                    nuevoPuntaje += 10;
                    nuevoNumCom += 1;
                    nuevoMapa[nuevoPacman.Posicion.Item2, nuevoPacman.Posicion.Item1] = Constantes.CAMINO;
                    if (nuevoNumCom == ComidaMax)
                    {
                        nuevoEst = EstadoJuego.GANAR;
                        nuevoPuntaje += 99999;
                    }
                }
                if (nuevoMapa[nuevoPacman.Posicion.Item2, nuevoPacman.Posicion.Item1] == Constantes.PODER)
                {
                    foreach (Fantasma f in nuevosFs)
                    {
                        f.Estado = EstadoFantasma.COMIBLE;
                        f.ModoIA = ModoIA.ESCAPE;
                    }
                    nuevoPuntaje += 50;
                    nuevoPoderActivado = true;
                    nuevoMapa[nuevoPacman.Posicion.Item2, nuevoPacman.Posicion.Item1] = Constantes.CAMINO;
                }
            }
            //else
            //{
                
                
            //}

            foreach (Fantasma f in nuevosFs.Where(x => x.Posicion.Item1 == nuevoPacman.Posicion.Item1 && x.Posicion.Item2 == nuevoPacman.Posicion.Item2))
            {
                if (f.Estado == EstadoFantasma.COMIBLE)
                {
                    f.Estado = EstadoFantasma.COMIDO;
                    nuevoPuntaje += 100;
                }
                if (f.Estado == EstadoFantasma.NORMAL)
                {
                    nuevoEst = EstadoJuego.MUERTE;
                    nuevoPuntaje -= 99999;
                }
            }
            if(nuevoPoderActivado)
            {
                if(nuevoDuracionPoder >= 150)
                {
                    foreach (Fantasma f in nuevosFs)
                    {
                        f.Estado = EstadoFantasma.NORMAL;
                        f.ModoIA = ModoIA.CAZA;
                    }
                    nuevoDuracionPoder = 0;
                    nuevoPoderActivado = false;
                }
                ++nuevoDuracionPoder;
            }

            foreach (Fantasma f in nuevosFs)
            {
                Direccion mover = escogerMovimiento(nuevoPacman, f, nuevoModoFs);

                int dfx = (mover == Direccion.DERECHA) ? 1 : ((mover == Direccion.IZQUIERDA) ? -1 : 0);
                int dfy = (mover == Direccion.ABAJO) ? 1 : ((mover == Direccion.ARRIBA) ? -1 : 0);
                Tuple<int, int> posicionFutura = new Tuple<int, int>(f.Posicion.Item1 + dfx, f.Posicion.Item2 + dfy);

                if (f.Posicion.Item1 == 0 && dx == -1) //Portal Izquierdo
                    f.Posicion = new Tuple<int, int>(Constantes.ANCHO - 1, f.Posicion.Item2);
                if (f.Posicion.Item1 == Constantes.ANCHO - 1 && dx == 1) //Portal Derecho
                    f.Posicion = new Tuple<int, int>(0, f.Posicion.Item2);

                if (!hayMuro(posicionFutura, nuevoMapa))
                    f.Posicion = posicionFutura;
            }

            foreach (Fantasma f in nuevosFs.Where(x => x.Posicion.Item1 == nuevoPacman.Posicion.Item1 && x.Posicion.Item2 == nuevoPacman.Posicion.Item2))
            {
                if (f.Estado == EstadoFantasma.COMIBLE)
                {
                    f.Estado = EstadoFantasma.COMIDO;
                    nuevoPuntaje += 100;
                }
                if (f.Estado == EstadoFantasma.NORMAL)
                {
                    nuevoEst = EstadoJuego.MUERTE;
                    nuevoPuntaje -= 99999;
                }
            }

            estado.Pacman = nuevoPacman;
            estado.Puntaje = nuevoPuntaje;
            estado.MapaActual = nuevoMapa;
            estado.EstadoJuego = nuevoEst;
            estado.NumComidos = nuevoNumCom;
            estado.Fantasmas = nuevosFs;
            estado.ModoFantasmas = nuevoModoFs;
            return estado;
        }

        public void minimax(int depth)
        {
            int masAltoValor = Int32.MinValue;
            int masBajoValor = Int32.MaxValue;
            int valorActual;
            Direccion mover = Direccion.NULL;

            List<Direccion> movLegales = obtPacmanMovs(Pacman, Mapa);
            
            foreach(Direccion d in movLegales)
            {
                Estado nuevoEstado = genNuevoEstado(d, 0, Pacman, Fantasmas, Puntaje, Mapa, EstadoJuego, NumComidos, ModoFantasmas.AGRESIVO, poderActivado, contadorPoder);

                valorActual = max(nuevoEstado, depth - 1);

                if(valorActual >= masAltoValor)
                {
                    masAltoValor = valorActual;
                    mover = d;
                }
            }

            Pacman.Direccion = mover;
        }

        private int max(Estado estado, int depth)
        {
            if (depth == 0 || estado.EstadoJuego == EstadoJuego.MUERTE)
                return estado.Puntaje;

            int masAltoValor = Int32.MinValue;
            int valorActual;

            List<Direccion> movLegales = obtPacmanMovs(estado.Pacman, estado.MapaActual);
            foreach(Direccion d in movLegales)
            {
                Estado nuevoEstado = genNuevoEstado(d, 0, estado.Pacman, estado.Fantasmas, estado.Puntaje, estado.MapaActual, estado.EstadoJuego, estado.NumComidos, ModoFantasmas.AGRESIVO, estado.PoderActivado, estado.DuracionPoder);

                //valorActual = min(nuevoEstado, depth - 1, 1);
                valorActual = max(nuevoEstado, depth - 1);

                if (valorActual >= masAltoValor)
                    masAltoValor = valorActual;
            }
            
            return masAltoValor;
        }

        private int min(Estado estado, int depth, int idF)
        {
            if (depth == 0 || estado.EstadoJuego == EstadoJuego.MUERTE)
                return estado.Puntaje;

            int masBajoValor = Int32.MaxValue;

            List<Direccion> movLegales = obtFantasmaMovs(estado.Fantasmas[idF - 1], estado.MapaActual);
            foreach(Direccion d in movLegales)
            {
                if(idF == 4)
                {
                    Estado nuevoEstado = genNuevoEstado(d, idF, estado.Pacman, estado.Fantasmas, estado.Puntaje, estado.MapaActual, estado.EstadoJuego, estado.NumComidos, estado.ModoFantasmas, estado.PoderActivado, estado.DuracionPoder);
                    masBajoValor = Math.Min(masBajoValor, max(estado, depth - 1));
                }
                else
                {
                    Estado nuevoEstado = genNuevoEstado(d, idF + 1, estado.Pacman, estado.Fantasmas, estado.Puntaje, estado.MapaActual, estado.EstadoJuego, estado.NumComidos, estado.ModoFantasmas, estado.PoderActivado, estado.DuracionPoder);
                    masBajoValor = Math.Min(masBajoValor, min(estado, depth, idF + 1));
                }
            }

            return masBajoValor;
        }
        private int min(Estado estado, int depth)
        {
            if (depth == 0 || estado.EstadoJuego == EstadoJuego.MUERTE)
                return estado.Puntaje;

            int masBajoValor = Int32.MaxValue;
            int valorActual;

            List<Direccion> movLegales = obtPacmanMovs(estado.Pacman, estado.MapaActual);
            foreach (Direccion d in movLegales)
            {
                Estado nuevoEstado = genNuevoEstado(d, 0, estado.Pacman, estado.Fantasmas, estado.Puntaje, estado.MapaActual, estado.EstadoJuego, estado.NumComidos, ModoFantasmas.AGRESIVO, estado.PoderActivado, estado.DuracionPoder);

                //valorActual = min(nuevoEstado, depth - 1, 1);
                valorActual = max(nuevoEstado, depth - 1);

                if (valorActual <= masBajoValor)
                    masBajoValor = valorActual;
            }

            return masBajoValor;
        }
        #endregion

        /// <summary>
        /// OPERACIONES PARA CONTROLES MANUALES POR PARTE DEL JUGADOR
        /// </summary>
        #region ---CONTROLES---
        //Funcion que para evitar cambios de dirección en el movimiento del Pacman en caso haya un muro (característica del Juego Original)
        public void cambiarDireccionPacman(Direccion direccion)
        {
            int dx = (direccion == Direccion.DERECHA) ? 1 : ((direccion == Direccion.IZQUIERDA) ? -1 : 0);
            int dy = (direccion == Direccion.ABAJO) ? 1 : ((direccion == Direccion.ARRIBA) ? -1 : 0);
            Tuple<int, int> posicionFutura = new Tuple<int, int>(Pacman.Posicion.Item1 + dx, Pacman.Posicion.Item2 + dy);

            if (movimientoValido(posicionFutura))
                this.Pacman.Direccion = direccion;
        }
        #endregion
    }
}
