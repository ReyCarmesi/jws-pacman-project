﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace JWS_PACMAN
{
    public class Fantasma
    {
        public Tuple<int, int> Posicion { get; set; }
        public Personalidad Personalidad { get; set; }
        public Direccion Direccion { get; set; }
        public EstadoFantasma Estado { get; set; }
        public V UltimoVertice { get; set; }
        public ModoIA ModoIA { get; set; }
        public Tuple<int, int> PosicionObjetivo { get; set; }
        int anim;

        public Fantasma(Personalidad Personalidad, Tuple<int,int> Posicion)
        {
            this.Personalidad = Personalidad;
            this.Posicion = Posicion;
            this.Direccion = Direccion.ARRIBA;
            this.UltimoVertice = null;
            this.PosicionObjetivo = new Tuple<int, int>(0, 0);
            this.Estado = EstadoFantasma.NORMAL;
            anim = 1;
        }

        public void dibujar(Graphics g)
        {
            String nombre = Personalidad.ToString(), dir = "";

            if (Direccion == Direccion.DERECHA) dir = "D";
            else if (Direccion == Direccion.IZQUIERDA) dir = "I";
            else if (Direccion == Direccion.ABAJO) dir = "AB";
            else if (Direccion == Direccion.ARRIBA) dir = "AR";
            else
                dir = "D";

            if (Estado == EstadoFantasma.COMIBLE) { nombre = "B"; dir = ""; }
            if (Estado == EstadoFantasma.COMIDO) { nombre = "O"; anim = 1; }

            String ruta = "pacman//" + nombre + "_" + dir + anim.ToString() + ".png"; //Ruta que es construida
            
            g.DrawImage(Image.FromFile(ruta), Posicion.Item1 * 15, Posicion.Item2 * 15, 15, 15);

            //Contador que hace la animación
            ++anim;
            if (anim > 2) anim = 1;
        }
    }
}
