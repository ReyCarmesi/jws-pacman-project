﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JWS_PACMAN
{
    public partial class Form1 : Form
    {
        Gestor gestor;
        bool MapaGenerado;

        public Form1()
        {
            InitializeComponent();
            gestor = new Gestor();
            cmbProfundidad.SelectedIndex = 0;
            rbCompEstandar.Checked = true;
            rbDistManhattan.Checked = true;
            MapaGenerado = false;
            cbMapaClasico.Checked = true;
        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            if (!MapaGenerado)
            {
                MessageBox.Show("Por favor genere un mapa antes de jugar", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            gestor.PacmanBotActivado = cbIA.Checked;
            timer1.Enabled = true;
        }
        private void btnPausar_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void btnCrearNivel_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            pnlMapa.CreateGraphics().Clear(Color.Black);
            gestor.MapaClasico = cbMapaClasico.Checked;
            gestor.generarMapa();
            gestor.dibujar(pnlMapa.CreateGraphics());
            gestor.reiniciarJuego();
            
            MapaGenerado = true;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = pnlMapa.CreateGraphics();
            BufferedGraphicsContext context = BufferedGraphicsManager.Current;
            BufferedGraphics buffer = context.Allocate(g, pnlMapa.DisplayRectangle);
            //Panel de Informacion
            Graphics g2 = pnlInfo.CreateGraphics();
            BufferedGraphics buffer2 = context.Allocate(g2, pnlInfo.DisplayRectangle);
            
            buffer2.Graphics.Clear(Color.Black);
            buffer.Graphics.Clear(Color.Black);

            //Opciones de Mapa
            gestor.mostrarVertices = cbGraficarVertices.Checked;
            gestor.mostrarObjetivos = cbMostrarObjetivos.Checked;
            gestor.PacmanBotActivado = cbIA.Checked;
            gestor.Profundidad = Convert.ToInt32(cmbProfundidad.SelectedItem);

            //Comportamiento de los fantasmas
            if (!rbCompEstandar.Checked)
            {
                if (rbCompAleatorio.Checked) gestor.ModoFantasmas = ModoFantasmas.ALEATORIO;
                if (rbCompAgresivo.Checked) gestor.ModoFantasmas = ModoFantasmas.AGRESIVO;
            }
            else
                gestor.ModoFantasmas = ModoFantasmas.ESTANDAR;

            //Modo de acercamiento
            if (rbDistEuclidiana.Checked) gestor.ModoAcercamiento = ModoAcercamiento.EUCLIDES;
            if (rbDistManhattan.Checked) gestor.ModoAcercamiento = ModoAcercamiento.MANHATTAN;

            if (gestor.EstadoJuego == EstadoJuego.JUEGO)
            {
                gestor.moverTodo(); 
                gestor.dibujar(buffer.Graphics);
            }
            else if(gestor.EstadoJuego == EstadoJuego.GANAR)
            {
                buffer.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(80, Color.Yellow)), 0, 0, pnlMapa.Width, pnlMapa.Height);
                buffer.Graphics.DrawString("GANASTE", new Font("Lucida Console", 10), Brushes.White, pnlMapa.Width / 2 - 10, pnlMapa.Height / 2 - 10);
                timer1.Enabled = false;
            }
            else if(gestor.EstadoJuego == EstadoJuego.MUERTE)
            {
                buffer.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(80, Color.Red)), 0, 0, pnlMapa.Width, pnlMapa.Height);
                buffer.Graphics.DrawString("PERDISTE", new Font("Lucida Console", 10), Brushes.White, pnlMapa.Width / 2 - 10, pnlMapa.Height / 2 - 10);
                timer1.Enabled = false;
            }

            buffer.Graphics.DrawString("PUNTAJE: " + gestor.Puntaje, new Font("Lucida Console", 15), Brushes.White, 0, Constantes.LARGO * 15 + 20);

            int contador = 0;
            foreach(Fantasma f in gestor.Fantasmas)
            {
                Color color = Color.White;

                if (f.Personalidad == Personalidad.BLINKY) color = Color.Red;
                if (f.Personalidad == Personalidad.INKY) color = Color.Aquamarine;
                if (f.Personalidad == Personalidad.CLYDE) color = Color.Orange;
                if (f.Personalidad == Personalidad.PINKY) color = Color.Pink;
                

                buffer2.Graphics.DrawString("<><><>", new Font("Lucida Console", 9), new SolidBrush(color), 0, contador);
                buffer2.Graphics.DrawImage(Image.FromFile("pacman//" + f.Personalidad.ToString() + "_D1.png"), 50, contador, 12, 12);
                buffer2.Graphics.DrawString(f.Personalidad.ToString() + "<><><>", new Font("Lucida Console", 9), new SolidBrush(color), 63, contador);
                buffer2.Graphics.DrawString("Posicion: (" + f.Posicion.Item1.ToString() + ", " + f.Posicion.Item2.ToString() + ")", new Font("Arial", 9), new SolidBrush(color), 0, contador + 10);
                buffer2.Graphics.DrawString("Direccion: " + f.Direccion.ToString(), new Font("Lucida Console", 9), new SolidBrush(color), 0, contador + 25);
                buffer2.Graphics.DrawString("IA: " + f.ModoIA.ToString(), new Font("Lucida Console", 9), new SolidBrush(color), 0, contador + 35);
                buffer2.Graphics.DrawString("Estado: " + f.Estado.ToString(), new Font("Lucida Console", 9), new SolidBrush(color), 0, contador + 45);

                contador += 80;
            }

            buffer2.Graphics.DrawString("<><><>", new Font("Lucida Console", 9), new SolidBrush(Color.Yellow), 0, contador);
            buffer2.Graphics.DrawImage(Image.FromFile("pacman//pacman_D1.png"), 50, contador, 12, 12);
            buffer2.Graphics.DrawString("PACMAN<><><>", new Font("Lucida Console", 9), new SolidBrush(Color.Yellow), 63, contador);
            buffer2.Graphics.DrawString("Posicion: (" + gestor.Pacman.Posicion.Item1.ToString() + ", " + gestor.Pacman.Posicion.Item2.ToString() + ")", new Font("Arial", 9), new SolidBrush(Color.Yellow), 0, contador + 10);
            buffer2.Graphics.DrawString("Direccion: " + gestor.Pacman.Direccion.ToString(), new Font("Lucida Console", 9), new SolidBrush(Color.Yellow), 0, contador + 25);

            buffer2.Graphics.DrawString("Modo Juego: " + gestor.ModoFantasmas.ToString(), new Font("Lucida Console", 9), new SolidBrush(Color.Yellow), 0, contador + 60);
            buffer2.Graphics.DrawString("Duración Poder: " + ((gestor.poderActivado) ? (150 - gestor.contadorPoder).ToString() : "0"), new Font("Lucida Console", 9), new SolidBrush(Color.Yellow), 0, contador + 70);
            
            buffer.Render(g);
            buffer2.Render(g2);
        }

        private void pnlMapa_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
                gestor.cambiarDireccionPacman(Direccion.ABAJO);
            if (e.KeyCode == Keys.W)
                gestor.cambiarDireccionPacman(Direccion.ARRIBA);
            if (e.KeyCode == Keys.A)
                gestor.cambiarDireccionPacman(Direccion.IZQUIERDA);
            if (e.KeyCode == Keys.D)
                gestor.cambiarDireccionPacman(Direccion.DERECHA);
            if (e.KeyCode == Keys.Space)
                gestor.Pacman.Direccion = Direccion.NULL;
        }
    }
}
