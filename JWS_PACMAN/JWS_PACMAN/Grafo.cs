﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWS_PACMAN
{
    public class E
    {
        public Direccion direccion { get; set; }
        public V vertice { get; set; }
        public E(V vertice, Direccion direccion)
        {
            this.direccion = direccion;
            //this.vertice = new V(valor);
            this.vertice = vertice;
        }
    }

    public class V
    {
        public List<E> anexos { get; set; }
        public Tuple<int, int> valor { get; set; }

        public V(Tuple<int, int> valor)
        {
            this.valor = valor;
            this.anexos = new List<E>();
        }
    }
}
