﻿namespace JWS_PACMAN
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlMapa = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnJugar = new System.Windows.Forms.Button();
            this.btnPausar = new System.Windows.Forms.Button();
            this.btnCrearNivel = new System.Windows.Forms.Button();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbCompEstandar = new System.Windows.Forms.RadioButton();
            this.rbCompAleatorio = new System.Windows.Forms.RadioButton();
            this.rbCompAgresivo = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbDistManhattan = new System.Windows.Forms.RadioButton();
            this.rbDistEuclidiana = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbProfundidad = new System.Windows.Forms.ComboBox();
            this.cbIA = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbMapaClasico = new System.Windows.Forms.CheckBox();
            this.cbMostrarObjetivos = new System.Windows.Forms.CheckBox();
            this.cbGraficarVertices = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMapa
            // 
            this.pnlMapa.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.pnlMapa.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlMapa.Location = new System.Drawing.Point(12, 12);
            this.pnlMapa.Name = "pnlMapa";
            this.pnlMapa.Size = new System.Drawing.Size(600, 634);
            this.pnlMapa.TabIndex = 0;
            this.pnlMapa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlMapa_MouseClick);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnJugar
            // 
            this.btnJugar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJugar.Image = ((System.Drawing.Image)(resources.GetObject("btnJugar.Image")));
            this.btnJugar.Location = new System.Drawing.Point(845, 501);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(222, 44);
            this.btnJugar.TabIndex = 1;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // btnPausar
            // 
            this.btnPausar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPausar.Image = ((System.Drawing.Image)(resources.GetObject("btnPausar.Image")));
            this.btnPausar.Location = new System.Drawing.Point(845, 551);
            this.btnPausar.Name = "btnPausar";
            this.btnPausar.Size = new System.Drawing.Size(222, 44);
            this.btnPausar.TabIndex = 2;
            this.btnPausar.Text = "Pausar";
            this.btnPausar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPausar.UseVisualStyleBackColor = true;
            this.btnPausar.Click += new System.EventHandler(this.btnPausar_Click);
            // 
            // btnCrearNivel
            // 
            this.btnCrearNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearNivel.Image = ((System.Drawing.Image)(resources.GetObject("btnCrearNivel.Image")));
            this.btnCrearNivel.Location = new System.Drawing.Point(845, 451);
            this.btnCrearNivel.Name = "btnCrearNivel";
            this.btnCrearNivel.Size = new System.Drawing.Size(222, 44);
            this.btnCrearNivel.TabIndex = 3;
            this.btnCrearNivel.Text = "Crear Nivel";
            this.btnCrearNivel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCrearNivel.UseVisualStyleBackColor = true;
            this.btnCrearNivel.Click += new System.EventHandler(this.btnCrearNivel_Click);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlInfo.Location = new System.Drawing.Point(618, 12);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(215, 634);
            this.pnlInfo.TabIndex = 4;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(845, 601);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(222, 45);
            this.btnSalir.TabIndex = 5;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(839, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 227);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones de los Enemigos";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbCompEstandar);
            this.groupBox3.Controls.Add(this.rbCompAleatorio);
            this.groupBox3.Controls.Add(this.rbCompAgresivo);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(216, 110);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Comportamiento";
            // 
            // rbCompEstandar
            // 
            this.rbCompEstandar.AutoSize = true;
            this.rbCompEstandar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCompEstandar.Location = new System.Drawing.Point(40, 50);
            this.rbCompEstandar.Name = "rbCompEstandar";
            this.rbCompEstandar.Size = new System.Drawing.Size(86, 21);
            this.rbCompEstandar.TabIndex = 2;
            this.rbCompEstandar.TabStop = true;
            this.rbCompEstandar.Text = "Estándar";
            this.rbCompEstandar.UseVisualStyleBackColor = true;
            // 
            // rbCompAleatorio
            // 
            this.rbCompAleatorio.AutoSize = true;
            this.rbCompAleatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCompAleatorio.Location = new System.Drawing.Point(40, 22);
            this.rbCompAleatorio.Name = "rbCompAleatorio";
            this.rbCompAleatorio.Size = new System.Drawing.Size(85, 21);
            this.rbCompAleatorio.TabIndex = 0;
            this.rbCompAleatorio.TabStop = true;
            this.rbCompAleatorio.Text = "Aleatorio";
            this.rbCompAleatorio.UseVisualStyleBackColor = true;
            // 
            // rbCompAgresivo
            // 
            this.rbCompAgresivo.AutoSize = true;
            this.rbCompAgresivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCompAgresivo.Location = new System.Drawing.Point(40, 78);
            this.rbCompAgresivo.Name = "rbCompAgresivo";
            this.rbCompAgresivo.Size = new System.Drawing.Size(84, 21);
            this.rbCompAgresivo.TabIndex = 3;
            this.rbCompAgresivo.TabStop = true;
            this.rbCompAgresivo.Text = "Agresivo";
            this.rbCompAgresivo.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbDistManhattan);
            this.groupBox4.Controls.Add(this.rbDistEuclidiana);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(6, 137);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(213, 84);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Modo de acercamiento";
            // 
            // rbDistManhattan
            // 
            this.rbDistManhattan.AutoSize = true;
            this.rbDistManhattan.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDistManhattan.Location = new System.Drawing.Point(40, 21);
            this.rbDistManhattan.Name = "rbDistManhattan";
            this.rbDistManhattan.Size = new System.Drawing.Size(96, 21);
            this.rbDistManhattan.TabIndex = 5;
            this.rbDistManhattan.TabStop = true;
            this.rbDistManhattan.Text = "Manhattan";
            this.rbDistManhattan.UseVisualStyleBackColor = true;
            // 
            // rbDistEuclidiana
            // 
            this.rbDistEuclidiana.AutoSize = true;
            this.rbDistEuclidiana.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDistEuclidiana.Location = new System.Drawing.Point(40, 46);
            this.rbDistEuclidiana.Name = "rbDistEuclidiana";
            this.rbDistEuclidiana.Size = new System.Drawing.Size(94, 21);
            this.rbDistEuclidiana.TabIndex = 6;
            this.rbDistEuclidiana.TabStop = true;
            this.rbDistEuclidiana.Text = "Euclidiano";
            this.rbDistEuclidiana.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cmbProfundidad);
            this.groupBox2.Controls.Add(this.cbIA);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(839, 245);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(228, 88);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones de Pacman";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(3, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Profundidad:";
            // 
            // cmbProfundidad
            // 
            this.cmbProfundidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfundidad.FormattingEnabled = true;
            this.cmbProfundidad.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cmbProfundidad.Location = new System.Drawing.Point(98, 48);
            this.cmbProfundidad.Name = "cmbProfundidad";
            this.cmbProfundidad.Size = new System.Drawing.Size(121, 24);
            this.cmbProfundidad.TabIndex = 1;
            // 
            // cbIA
            // 
            this.cbIA.AutoSize = true;
            this.cbIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIA.Location = new System.Drawing.Point(6, 21);
            this.cbIA.Name = "cbIA";
            this.cbIA.Size = new System.Drawing.Size(154, 21);
            this.cbIA.TabIndex = 0;
            this.cbIA.Text = "Inteligencia Artificial";
            this.cbIA.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbMapaClasico);
            this.groupBox5.Controls.Add(this.cbMostrarObjetivos);
            this.groupBox5.Controls.Add(this.cbGraficarVertices);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(839, 339);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(228, 106);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Mapa";
            // 
            // cbMapaClasico
            // 
            this.cbMapaClasico.AutoSize = true;
            this.cbMapaClasico.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMapaClasico.Location = new System.Drawing.Point(6, 75);
            this.cbMapaClasico.Name = "cbMapaClasico";
            this.cbMapaClasico.Size = new System.Drawing.Size(114, 21);
            this.cbMapaClasico.TabIndex = 2;
            this.cbMapaClasico.Text = "Mapa Clásico";
            this.cbMapaClasico.UseVisualStyleBackColor = true;
            // 
            // cbMostrarObjetivos
            // 
            this.cbMostrarObjetivos.AutoSize = true;
            this.cbMostrarObjetivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMostrarObjetivos.Location = new System.Drawing.Point(6, 48);
            this.cbMostrarObjetivos.Name = "cbMostrarObjetivos";
            this.cbMostrarObjetivos.Size = new System.Drawing.Size(207, 21);
            this.cbMostrarObjetivos.TabIndex = 1;
            this.cbMostrarObjetivos.Text = "Mostrar Objetivos Enemigos";
            this.cbMostrarObjetivos.UseVisualStyleBackColor = true;
            // 
            // cbGraficarVertices
            // 
            this.cbGraficarVertices.AutoSize = true;
            this.cbGraficarVertices.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGraficarVertices.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbGraficarVertices.Location = new System.Drawing.Point(6, 21);
            this.cbGraficarVertices.Name = "cbGraficarVertices";
            this.cbGraficarVertices.Size = new System.Drawing.Size(140, 21);
            this.cbGraficarVertices.TabIndex = 0;
            this.cbGraficarVertices.Text = "Mostrar Esquinas";
            this.cbGraficarVertices.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 658);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnCrearNivel);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.btnPausar);
            this.Controls.Add(this.pnlMapa);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "JWS Pacman";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMapa;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.Button btnPausar;
        private System.Windows.Forms.Button btnCrearNivel;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbProfundidad;
        private System.Windows.Forms.CheckBox cbIA;
        private System.Windows.Forms.RadioButton rbCompAleatorio;
        private System.Windows.Forms.RadioButton rbDistEuclidiana;
        private System.Windows.Forms.RadioButton rbDistManhattan;
        private System.Windows.Forms.RadioButton rbCompAgresivo;
        private System.Windows.Forms.RadioButton rbCompEstandar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox cbGraficarVertices;
        private System.Windows.Forms.CheckBox cbMostrarObjetivos;
        private System.Windows.Forms.CheckBox cbMapaClasico;
    }
}

