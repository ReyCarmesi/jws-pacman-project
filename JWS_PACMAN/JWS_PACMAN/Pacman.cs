﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace JWS_PACMAN
{
    public class Pacman
    {
        public Tuple<int, int> Posicion { get; set; }
        public Direccion Direccion { get; set; }
        private int anim;

        public Pacman(Direccion Direccion, Tuple<int, int> Posicion) 
        {
            this.Direccion = Direccion;
            this.Posicion = Posicion;
            anim = 0;
        }

        public void dibujar(Graphics g)
        {
            String dir = "";

            if (Direccion == Direccion.ARRIBA) dir = "AR";
            else if (Direccion == Direccion.ABAJO) dir = "AB";
            else if (Direccion == Direccion.IZQUIERDA) dir = "I";
            else if (Direccion == Direccion.DERECHA) dir = "D";
            else dir = "D";

            if (anim == 0) dir = "";
            String ruta = "pacman//PACMAN_" + dir + anim.ToString() + ".png"; //Ruta que es construida
            g.DrawImage(Image.FromFile(ruta), Posicion.Item1 * 15, Posicion.Item2 * 15, 15, 15);
            
            //Contador que hace la animación
            ++anim;
            if (anim > 2) anim = 0;
        }
    }
}
