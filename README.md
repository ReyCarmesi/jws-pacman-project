# PRESENTACION #
Bienvenido...

### Nota para el Profesor ###
* Las principales clases se encuentran en la opción Source -> carpeta JWS Pacman / JWS Pacman:
* Constantes.cs
* Estado.cs
* Fantasma.cs
* Form1.cs
* Gestor.cs
* Grafo.cs
* Mapas.cs
* Pacman.cs

### Datos Generales ##

* Objetivo: Diseñar una aplicación que emplee algoritmos codiciosos y de búsqueda informada para la implementación de una inteligencia artificial en el clásico juego Pacman. 
* Lenguaje: C#
* Version: 1.0

### Datos del Curso ###

* Curso: Complejidad Algorítmica
* Profesor: Luis Canaval
* Sección: CC41
* Proyecto: Trabajo Final

### Contribuyentes ###

* Andrea Valentín Dávila
* Jason Hubert Almandroz
* Jeffrey Rojas Montes